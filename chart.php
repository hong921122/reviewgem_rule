<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>
    <script src="./js/keyboard.js"></script>
    <script src="./js/click_event.js"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        window.onload = function() {
            loadChart(true);
        };

        function loadChart(onload) {
            var menu_name = "";
            if (onload == undefined) {
                menu_name = event.toElement.innerHTML;
                $('#btn-chart-type').html(menu_name);
            } else {
                menu_name = "Total Rule Count";
            }
            var file_name = menu_name.toLowerCase().replace(/ /g, "_");

            $.post('./php/count_dic.php', { file_name:file_name}, function(chart_data){
                loadApi(chart_data, menu_name);
            });
        }

        function moveScroll() {
            var left = $(document).outerWidth() - document.getElementById("container").offsetWidth;
            $('body').scrollLeft(left);
        };

        function loadApi(chart_data, chart_name) {
            var callback = drawChart.bind(null, chart_data, chart_name);
            google.load("visualization", "1", {"callback" : callback, packages:["corechart"]});
        }

        function drawChart(chart_data, chart_name) {
            var data_array = JSON.parse(chart_data);
            for (var i = 1; i < data_array.length; i++) {
                data_array[i][0] = data_array[i][0].replace("2014-", "");
            }

            var data = setColumnsForGoogleChart(data_array);

//            document.getElementById("chart_div").setAttribute("style", "width:" + data_array.length * 100 + "px; height:600px;");
            var left = $(document).outerWidth() - document.getElementById("container").offsetWidth;
            $('body').scrollLeft(left);

            var options = {
                title: chart_name,
                legend: { position: 'top', maxLines: 3 },
                bar: { groupWidth: '75%' },
                chartArea: { left:50, top:50, width:"100%", height:400 },
                isStacked: true,
                seriesType: "bars",
                series: {4: {type: "line"}}
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }

        function getValueAt(column, dataTable, row) {
            var value = dataTable.getFormattedValue(row, column);
            if (value == "0") return "";
            else return value;
        }

        function setColumnsForGoogleChart(data_array) {
            var data = google.visualization.arrayToDataTable(data_array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0,
                1, { calc: getValueAt.bind(undefined, 1),
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation" },
                2, { calc: getValueAt.bind(undefined, 2),
                    sourceColumn: 2,
                    type: "string",
                    role: "annotation" },
                3, { calc: getValueAt.bind(undefined, 3),
                    sourceColumn: 3,
                    type: "string",
                    role: "annotation" },
                4, { calc: getValueAt.bind(undefined, 4),
                    sourceColumn: 4,
                    type: "string",
                    role: "annotation" },
                5, { calc: getValueAt.bind(undefined, 5),
                    sourceColumn: 5,
                    type: "string",
                    role: "annotation" }
            ]);
            return view;
        }
    </script>
</head>
<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 40px 15px;
    }

</style>

<body id="body" onload="moveScroll()">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./main.php">소마데모</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="./main.php">Home</a></li>
                <li><a href="./crawl_view.php">crawl_view</a></li>
                <li><a href="./manage.php">manage</a></li>
                <li><a href="./manage_rule.php">rule managing</a></li>
                <li><a href="./review_semantic_result.php">semantic result</a></li>
                <li><a href="./rule_coverage.php">rule coverage</a></li>
                <li class="active"><a href="./chart.php">chart</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-chart-type" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
        Today Rule Count<span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a onclick="loadChart()">Total Rule Count</a></li>
        <li><a onclick="loadChart()">Total Submit Count</a></li>
        <li><a onclick="loadChart()">Total Dic Target Count</a></li>
        <li><a onclick="loadChart()">Total Dic Attr Count</a></li>
        <li><a onclick="loadChart()">Total Dic Sentiment Count</a></li>
        <li><a onclick="loadChart()">Today Rule Count</a></li>
        <li><a onclick="loadChart()">Today Submit Count</a></li>
        <li><a onclick="loadChart()">Today Dic Target Count</a></li>
        <li><a onclick="loadChart()">Today Dic Attr Count</a></li>
        <li><a onclick="loadChart()">Today Dic Sentiment Count</a></li>
    </ul>
</div>

<div id="container">
    <div id="chart_div" style="width: 100%; height: 600px;"></div>
</div>

</body>
</html>

