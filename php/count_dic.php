<?
$dir = "/var/www/static/dictionary_counter/";
$path_count_by_user = $dir . $_POST['file_name'] . "_by_user.json";
$path_count = $dir . $_POST['file_name'] . ".json";

$file_count_by_user = fopen($path_count_by_user, "r");
$file_count = fopen($path_count, "r");

if ($file_count && $file_count_by_user) {
    while ( ($str = fgets($file_count_by_user)) != false) {
        $res_str_count_by_user .= $str;
    }
    while ( ($str = fgets($file_count)) != false) {
        $res_str_count .= $str;
    }

    $json_object = json_decode($res_str_count_by_user, true);
    $date_keys = array_keys($json_object);
    sort($date_keys);
    $chart_array = array();
    $chart_array[0] = array('날짜', '박진철', '유은상', '홍석준', '미지정', '총 갯수');
    $number_of_date = 0;
    foreach($date_keys as $date_key) {
        $number_of_date++;
        $chart_array[$number_of_date] = array($date_key, 0, 0, 0, 0, 0);
        $count_for_users = $json_object[$date_key];
        $user_keys = array_keys($count_for_users);
        foreach($user_keys as $user_key) {
            $number_of_value = $count_for_users[$user_key];
            if ($user_key == '박진철') {
                $chart_array[$number_of_date][1] = $number_of_value;
            } else if ($user_key == '유은상') {
                $chart_array[$number_of_date][2] = $number_of_value;
            } else if ($user_key == '홍석준') {
                $chart_array[$number_of_date][3] = $number_of_value;
            } else {
                $chart_array[$number_of_date][4] += $number_of_value;
            }
        }
    }

    $json_object = json_decode($res_str_count, true);
    $date_keys = array_keys($json_object);
    sort($date_keys);
    $number_of_date = 0;
    foreach($date_keys as $date_key) {
        $number_of_date++;
        $chart_array[$number_of_date][5] = $json_object[$date_key];
        $rest = $chart_array[$number_of_date][5] - $chart_array[$number_of_date][1] - $chart_array[$number_of_date][2] - $chart_array[$number_of_date][3];
        $chart_array[$number_of_date][1] += (int)($rest / 3);
        $chart_array[$number_of_date][2] += (int)($rest / 3);
        $chart_array[$number_of_date][3] += $rest - (int)(($rest / 3) * 2);
        $chart_array[$number_of_date][4] = 0;
    }

    $chart_array_count = count($chart_array);

    if ($chart_array_count > 20) {
        $length = $chart_array_count - 20;
        $chart_array_keys = array_keys($chart_array);
        $chart_return_array[0] = $chart_array[0];
        for ($i = 0; $i < $chart_array_count; $i++) {
            $chart_return_array[$i] = $chart_array[$chart_array_keys[$i]];
        # for ($i = $length; $i < $length + 20; $i++) {
            # $chart_return_array[$i - $length + 1] = $chart_array[$chart_array_keys[$i]];
        }
        echo json_encode($chart_return_array);
    } else {
        echo json_encode($chart_array);
    }
} else {
    echo "file does not exist!";
}
fclose($file_count);
fclose($file_count_by_user);
?>

