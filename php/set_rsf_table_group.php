<?
/**
 * Created by IntelliJ IDEA.
 * User: Arthur
 * Date: 2014. 4. 29.
 * Time: 오후 11:27
 */

function draw_review_table($review) {
    $html = "<label>리뷰 문서 정보</label>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td style=\"width:50px;\">crawling_uid</td>";
    $html .= "<td>원본 파일 위치</td>";
    $html .= "<td>리뷰 제목</td>";
    $html .= "<td>url</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $review['review_id'] . "</td>";
    $html .= "<td>" . $review['file_path'] . "</td>";
    $html .= "<td>" . $review['title'] . "</td>";
    $html .= "<td><a href=" . $review['url'] . " target=\"_blank\">" . $review['url'] . "</a></td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td colspan=\"4\">리뷰 내용</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td colspan=\"4\">" . $review['content'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    echo $html;
}

function draw_sentence_table($sentence) {
    $html = "<HR style=\"width:100%;height:10px;background-color:blue;\"/>";
    $html .= "<label>룰 적용 대상 문장 정보 - " . $sentence['sentence_id'] . "</label>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td>문장 원본</td>";
    $html .= "<td>어절 배열</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $sentence['sentence'] . "</td>";
    $html .= "<td>" . $sentence['eojeols'] . "</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>형태소 배열</td>";
    $html .= "<td>태그 배열</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $sentence['morphemes'] . "</td>";
    $html .= "<td>" . $sentence['tags'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    echo $html;
}

function draw_rule_table($review_id, $sentence_id, $rule) {
    $rule_result = $rule['compare_with_result'];
    $color = "yellow";
    if ($rule_result == 'correct') $color = 'green';
    else if ($rule_result == 'wrong') $color = 'yellow';
    else if ($rule_result == 'not a rule') $color = 'red';
    else if ($rule_result == 'no result') $color = 'grey';
    else if ($rule_result == 'not this attr') $color = 'orange';
    $html = "<HR style=\"width:100%;height:10px;background-color:" . $color . ";\"/>";
    $html .= "<label>룰 정보 - " . $rule['rule_id'] . "</label>";
    $html .= "<label>타겟(제품명) 정보</label>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td>형태소 배열</td>";
    $html .= "<td>태그 배열</td>";
    $html .= "<td>원본 저장 index</td>";
    $html .= "<td>수정된 압축 index</td>";
    $html .= "<td>매칭 index</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $rule['target'] . "</td>";
    $html .= "<td>" . $rule['target_tag'] . "</td>";
    $html .= "<td>" . $rule['original_target_index'] . "</td>";
    $html .= "<td>" . $rule['abs_target_index'] . "</td>";
    $html .= "<td>" . $rule['matched_indices']['target'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    $html .= "<label>속성(제품 특성) 정보</label>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td>형태소 배열</td>";
    $html .= "<td>태그 배열</td>";
    $html .= "<td>원본 저장 index</td>";
    $html .= "<td>수정된 압축 index</td>";
    $html .= "<td>매칭 index</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $rule['attr'] . "</td>";
    $html .= "<td>" . $rule['attr_tag'] . "</td>";
    $html .= "<td>" . $rule['original_attr_index'] . "</td>";
    $html .= "<td>" . $rule['abs_attr_index'] . "</td>";
    $html .= "<td>" . $rule['matched_indices']['attr'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    $html .= "<label>DB 원본 감성 정보</label>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td>형태소 배열</td>";
    $html .= "<td>태그 배열</td>";
    $html .= "<td>원본 저장 index</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $rule['sentiment'] . "</td>";
    $html .= "<td>" . $rule['sentiment_tag'] . "</td>";
    $html .= "<td>" . $rule['original_sentiment_index'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    $html .= "<label>수정된 감성 정보</label>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td>형태소 배열</td>";
    $html .= "<td>태그 배열</td>";
    $html .= "<td>원본 저장 index</td>";
    $html .= "<td>수정된 압축 index</td>";
    $html .= "<td>매칭 index</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $rule['modified_sentiment'] . "</td>";
    $html .= "<td>" . $rule['modified_sentiment_tag'] . "</td>";
    $html .= "<td>" . $rule['original_modified_sentiment_index'] . "</td>";
    $html .= "<td>" . $rule['abs_modified_sentiment_index'] . "</td>";
    $html .= "<td>" . $rule['matched_indices']['sentiment'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    $rule['origin_attr'] = str_replace(" ", "_", $rule['origin_attr']);
    $id = $rule['origin_attr'] . '-' . $sentence_id . '-' . $rule['sentiment_type'];

    $html .= "<label>" . $rule['origin_attr'] . "</label>";

    $html .= "<div id=\"" . $id . "\" class=\"btn-group\" style=\"margin-top:10px;margin-left:5px;\">";

    $html .= "<button id=\"" . $id . "button\" class=\"btn btn-default btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">";
    if ($rule['sentiment_type'] == 0)
        $html .= "positive";
    else if ($rule['sentiment_type'] == 1)
        $html .= "negative";
    else if ($rule['sentiment_type'] == 2)
        $html .= "neutral";
    $html .= "</button>";
    $html .= "<ul class=\"dropdown-menu\">";
    $html .= "<li><a onclick=\"updateResult()\">positive</a></li>";
    $html .= "<li><a onclick=\"updateResult()\">negative</a></li>";
    $html .= "<li><a onclick=\"updateResult()\">neutral</a></li>";
    $html .= "<li><a onclick=\"updateResult()\">not this attr</a></li>";
    $html .= "<li><a onclick=\"updateResult()\">not a rule</a></li>";
    $html .= "</ul>";

    $html .= "</div>";

    $rule['origin_attr'] = str_replace("_", " ", $rule['origin_attr']);

    $html .= "<div id=\"" . $id . "\" style=\"margin-top:10px;margin-left:5px;\">";
    $html .= "<button type=\"submit\" onclick=saveRule() value=\"" . $id . "\">save</button>";
    $html .= "</div>";

    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";
    $html .= "<tr>";
    $html .= "<td>update</td>";
    $html .= "<td>id</td>";
    $html .= "<td>origin_attr</td>";
    $html .= "<td style=width:50px>sentiment_type</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td><button type=\"submit\" onclick=updateEvent() value=\"" . $rule['rule_id'] . "\">update</button></td>";
    $html .= "<td><input type=\"text\" class=\"" . $rule['rule_id'] . "\" value=\"" . $rule['rule_id'] . "\" readonly=\"readonly\" style=width:50px></td>";
    $html .= "<td><input type=\"text\" class=\"" . $rule['rule_id'] . "\" value=\"" . $rule['origin_attr'] . "\"></td>";
    $html .= "<td><input type=\"text\" class=\"" . $rule['rule_id'] . "\" value=\"" . $rule['sentiment_type'] . "\" style=width:50px></td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    echo $html;
}

$dir = "/var/www/static/match_check_result/";
$review_id = $_POST['review_id'];
$file_extension = ".json";
$file_path = $dir . $review_id . $file_extension;
$mode_array = $_POST['mode'];

$file = fopen($file_path, "r");
if($file) {
    while (($str = fgets($file)) != false)
        $res_str .= $str;
    $review_semantic_result = json_decode($res_str, true);
    $review = $review_semantic_result['review'];
    draw_review_table($review);
    $sentences = $review_semantic_result['sentences'];
    foreach ($sentences as $sentence) {
        $rules = $sentence['rules'];
        $rule_count = array();
        $total_count = 0;
        foreach ($rules as $rule) {
            $compare_with_result = $rule['compare_with_result'];
            foreach ($mode_array as $mode) {
                if ($compare_with_result == $mode) {
                    $rule_count[$mode]++;
                    $total_count++;
                }
            }
        }
        if ($total_count > 0) {
            draw_sentence_table($sentence);
            foreach ($mode_array as $mode) {
                if ($rule_count[$mode] > 0) {
                    $label = "";
                    if ($mode == 'no result') {
                        $label = "저장할 룰들";
                    } else if ($mode == 'wrong') {
                        $label = "잘못 매칭된 룰들";
                    } else if ($mode == 'not a rule') {
                        $label = "룰 문장이 아님";
                    } else if ($mode == 'not this attr') {
                        $label = "이 속성에 대한 룰 문장이 아님";
                    } else if ($mode == 'correct') {
                        $label = "맞게 매칭된 룰들";
                    }

                    echo "<label>" . $label . "</label>";
                    foreach ($rules as $rule) {
                        if ($rule['compare_with_result'] != $mode) continue;
                        echo "<div>";
                        draw_rule_table($review['review_id'], $sentence['sentence_id'], $rule);
                        echo "</div>";
                    }
                }

            }
        }
    }
}

fclose($file);
?>
