<?
/**
 * Created by IntelliJ IDEA.
 * User: Arthur
 * Date: 2014. 5. 13.
 * Time: 오전 10:42
 */

function draw_rule_table($rule) {
    $color = "gray";
    $html = "<HR style=\"width:100%;height:2px;background-color:" . $color . ";\"/>";
    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td style=\"width:80px\">매칭 갯수</td>";
    $html .= "<td style=\"width:120px\">저장 날짜</td>";
    $html .= "<td style=\"width:50px\">룰 ID</td>";
    $html .= "<td style=\"width:120px\">대표 속성</td>";
    $html .= "<td>원본 문장</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td style = \"font-weight:bold;font-size:150%;color:red\">" . $rule['count'] . "</td>";
    $html .= "<td>" . $rule['date'] . "</td>";
    $html .= "<td>" . $rule['id'] . "</td>";
    $html .= "<td>" . $rule['origin_attr'] . "</td>";
    $html .= "<td>" . $rule['sentence'] . "</td>";
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    $html .= "<table class=\"table table-striped\">";
    $html .= "<tbody>";

    $html .= "<tr>";
    $html .= "<td>타겟</td>";
    $html .= "<td>속성</td>";
    $html .= "<td>감성</td>";
    $html .= "<td style=\"width:120px\">긍부정 결과</td>";
    $html .= "</tr>";

    $html .= "<tr>";
    $html .= "<td>" . $rule['target'] . "</td>";
    $html .= "<td>" . $rule['attr'] . "</td>";
    $html .= "<td>" . $rule['sentiment_eojeol'] . "</td>";
    if ($rule['sentiment_type'] == 0) {
        $html .= "<td>Positive</td>";
    } else if ($rule['sentiment_type'] == 1) {
        $html .= "<td>Negative</td>";
    } else {
        $html .= "<td>Neutral</td>";
    }
    $html .= "</tr>";

    $html .= "</tbody>";
    $html .= "</table>";

    return $html;
}

$dir = $_POST['dir'];
$file_prefix_name = $_POST['prefix'];
$user_name = $_POST['user'];
$file_extension = ".json";
$file_path = $dir . $file_prefix_name . $user_name . $file_extension;

$file = fopen($file_path, "r");
if($file) {
    while (($str = fgets($file)) != false)
        $res_str .= $str;
    $rule_coverage_json = json_decode($res_str, true);
    $total_rule_count = $rule_coverage_json['total_count'];
    $rule_coverages = $rule_coverage_json['rule_coverages'];

    $html = "<label style=\"font-size:200%;color:red;\"> Total count : " . $total_rule_count . " </label>";
    foreach ($rule_coverages as $rule_coverage) {
        $html .= draw_rule_table($rule_coverage);
    }
    $return_array = array();
    $return_array['html'] = $html;
    if ($file_prefix_name == 'coverage_for_ruleid_') {
        $return_array['chart'] = array();
        $return_array['chart'][0] = array('id', 'count');
        $number_of_rows = count($rule_coverage_json['count_for_ruleid']);
        for ($i = 0; $i < $number_of_rows; $i++) {
            $rule_coverage_object = $rule_coverage_json['count_for_ruleid'][$i];
            if ($rule_coverage_object['count'] == '0' || $i > 20) break;
            $return_array['chart'][$i + 1] = array($rule_coverage_object['id'] . " ", $rule_coverage_object['count']);
        }
    } else if ($file_prefix_name == 'coverage_for_date_') {
        $return_array['chart'] = array();
        $return_array['chart'][0] = array('날짜', 'count');
        $date_keys = array_keys($rule_coverage_json['count_for_date']);
        rsort($date_keys);
        foreach($date_keys as $date_key) {
            if ($rule_coverage_json['count_for_date'][$date_key] == '0') continue;
            $number_of_date++;
            if ($number_of_date > 20) break;
            $return_array['chart'][$number_of_date] = array($date_key, $rule_coverage_json['count_for_date'][$date_key]);
        }
    }
    echo json_encode($return_array);
}
?>