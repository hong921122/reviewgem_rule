<?
/**
 * Created by IntelliJ IDEA.
 * User: Arthur
 * Date: 2014. 4. 29.
 * Time: 오후 10:58
 */

$dir = "/var/www/static/match_check_result";
$file_extension = ".json";

if (!$dh = @opendir($dir)) {
    return false;
}

$review_ids = array();

while(($file_name = readdir($dh)) != false) {
    if ($file_name == "." || $file_name == "..") continue;
    $file_name = str_replace($file_extension, "", $file_name);
    array_push($review_ids, $file_name);
}
foreach ($review_ids as $review_id) {
    echo "<li><a onclick=\"showReviewData()\">" . $review_id . "</li>";
}
?>
