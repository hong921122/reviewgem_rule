<?
/**
 * Created by IntelliJ IDEA.
 * User: Arthur
 * Date: 2014. 5. 13.
 * Time: 오전 10:03
 */

$dir = $_POST['dir'];
$file_prefix_name = $_POST['prefix'];
$file_extension = ".json";

if (!$dh = @opendir($dir)) {
    return false;
}

$user_names = array();

while(($file_name = readdir($dh)) != false) {
    if ($file_name == "." || $file_name == "..") continue;
    if (strlen(strpos($file_name, $file_prefix_name)) > 0 && strlen(strpos($file_name, $file_extension)) > 0) {
        $file_name = str_replace($file_prefix_name, "", $file_name);
        $file_name = str_replace($file_extension, "", $file_name);
        array_push($user_names, $file_name);
    }
}

echo json_encode($user_names);
?>

