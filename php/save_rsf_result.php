<?
/**
 * Created by IntelliJ IDEA.
 * User: Arthur
 * Date: 2014. 4. 30.
 * Time: 오후 8:23
 */

include "../include/dbinfo.php";

$review_id = $_POST['review_id'];
$sentence_id = $_POST['sentence_id'];
$sentiment_type = $_POST['sentiment_type'];
$origin_attr = $_POST['origin_attr'];

if ($sentiment_type == 'positive') {
    $sentiment_type = 0;
} else if ($sentiment_type == 'negative') {
    $sentiment_type = 1;
} else if ($sentiment_type == 'neutral') {
    $sentiment_type = 2;
} else if ($sentiment_type == 'not a rule') {
    $sentiment_type = -1;
    $origin_attr = null;
} else if ($sentiment_type == 'not this attr') {
    $sentiment_type = -2;
} else{
    $sentiment_type = -100;
}

$origin_attr = str_replace("_", " ", $origin_attr);

$query = "select count(*) as count from rule_matching_result where";
$query .= " review_id = " . $review_id;
$query .= " AND sentence_id = " . $sentence_id;
$query .= " AND sentiment_type = " . $sentiment_type;
if ($origin_attr != null)
    $query .= " AND origin_attr = \"" . $origin_attr . "\"";

$res = mysql_query($query);
$result = mysql_fetch_array($res);
$count = $result['count'];

if ($count == 0) {
    $query = "insert into rule_matching_result (review_id, sentence_id, sentiment_type,";
    if ($origin_attr != null) $query .= " origin_attr,";
    $query .= "date) ";
    $query .= "values (" . $review_id . "," . $sentence_id . "," . $sentiment_type . ",";
    if ($origin_attr != null) $query .= "\"" . $origin_attr . "\",";
    $query .= "now() )";
    $res = mysql_query($query);
    echo "success";
} else {
    echo "exits";
}

mysql_close();

?>