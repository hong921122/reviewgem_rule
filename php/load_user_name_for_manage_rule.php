<?

$dir = "/var/www/static/error_check/";
$file_prefix_name = "savedRuleErrorChecking_";
$file_extension = ".json";
$only_error = $_POST['only_error'];
if ($only_error == "false") {
    $file_prefix_name .= "all_";
} else {
    $file_prefix_name .= "only_error_";
}

if (!$dh = @opendir($dir)) {
    return false;
}

$user_names = array();

while(($file_name = readdir($dh)) != false) {
    if ($file_name == "." || $file_name == "..") continue;
    if (strlen(strpos($file_name, $file_prefix_name)) > 0 && strlen(strpos($file_name, $file_extension)) > 0) {
        $file_name = str_replace($file_prefix_name, "", $file_name);
        $file_name = str_replace($file_extension, "", $file_name);
        array_push($user_names, $file_name);
    }
}
echo json_encode($user_names);
?>

