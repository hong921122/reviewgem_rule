<?
include "../include/dbinfo.php";

function print_eojeol_array($eojeol_array) {
  echo "[";
  $numberOfEojeol = count($eojeol_array);
  for ($i = 0; $i < $numberOfEojeol; $i++) {
    $word = $eojeol_array[$i];
    if ($i != 0) echo ",";
    echo "<a name=\"wordlist\" draggable=\"true\" ondragstart=\"drag(event)\">". $word . "</a>";
  }
  echo "]";
  echo "<br>";
}

function print_eojeol_arrays($morphemes_array, $tags_array) {
  $numberOfSentence = count($morphemes_array);
  for ($i = 0; $i < $numberOfSentence; $i++) {
    $morphemes = $morphemes_array[$i];
    $tags = $tags_array[$i];
    echo "[";
    $numberOfMorpheme = count($morphemes);
    for ($j = 0; $j < $numberOfMorpheme; $j++) {
      $morpheme = $morphemes[$j];
      $tag = $tags[$j];
      if ($j != 0) echo ",";
      echo "<a name=\"wordlist\" draggable=\"true\" ondragstart=\"drag(event)\">". $morpheme . "</a>" . "/" . $tag;
    }
    echo "]";
  }
  echo "<br>";
}

$dir = "/var/www/nlp/web_morpheme_tool/";
$mor = $_POST['value'];
$res = mysql_fetch_array(mysql_query("select path from crawling where uid=" . $mor));

$path = $dir . substr(strrchr($res["path"], '/'), 1);

$file = fopen($path, "r");
if($file){
    while( ($str = fgets($file)) != false)
        $res_str .= $str;
$str = json_decode($res_str, true);

$i = 1;
echo var_dump($path) . "<br><br>";
foreach($str["morphemeAnalyze"] as $t){
    echo "<br>" . $i . ": <a onclick=\"setSentence(event)\" value=\"$i\">" . $t["sentence"] . "</a><br>";
    echo "[형태소/한글태그명] : ";
    print_eojeol_arrays($t["morphemes"], $t["koreantags"]);
    echo "[어절] : ";
    print_eojeol_array($t["eojeol"]);
    $i++;


}
}
fclose($file);
mysql_close();
?>

