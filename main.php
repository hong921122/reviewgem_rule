<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="starter-template.css" rel="stylesheet">


</head>
<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 40px 15px;
    }

</style>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./main.php">소마데모</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="./main.php">Home</a></li>
                <li><a href="./crawl_view.php">crawl_view</a></li>
                <li><a href="./manage.php">manage</a></li>
                <li><a href="./manage_rule.php">rule managing</a></li>
                <li><a href="./review_semantic_result.php">semantic result</a></li>
                <li><a href="./rule_coverage.php">rule coverage</a></li>
                <li><a href="./chart.php">chart</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container">
    <div class="template">
        <h1 style="text-align:center;margin-top:20px;">검색어를 입력하세요</h1>
        <h1 style="text-align:center;">뿌잉뿌잉</h1>
        <form method="post">
            <input type="text" class="form-control" placeholder="search input" name="keyword" style="margin-top:10px;">
            <input type="text" class="form-control" placeholder="name input" name="user" style="margin-top:10px;margin-bottom:10px;">
            <button type="submit" class="btn btn-default" style="margin-top:5px;">submit</button>
        </form>
    </div>
</div><!-- /.container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>
</body>
</html>

