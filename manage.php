<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>

</head>
<script type="text/javascript">
    $(document).ready(function(){
        refresh();
    });

    function updateEvent(event){
        var update_btn = document.getElementsByClassName(event.toElement.value);
        var t = $.post('./php/req.php', {id : update_btn[0].value, review_id : update_btn[1].value, sentence_id : update_btn[2].value, sentence : update_btn[3].value, origin_attr : update_btn[4].value, target : update_btn[5].value, attr : update_btn[6].value, sentiment_eojeol : update_btn[7].value, sentiment_type : update_btn[8].value });
        t.done(function(data){
            console.log(data);
        });
        refresh();
    }

    function deleteEvent(event){
        var val = event.toElement.value;
        $.post('./php/del.php', {id : event.toElement.value});
        refresh();
    }

    function refresh(){
        $('#info').html("");
        var posting = $.post('./php/manage.php');
        posting.done(function(data){
            $('#info').append(data);
        });
    }
</script>

<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 10px 15px;
    }
    .content {
        width: 500px;
        height: 500px;
    }
</style>

<body id="body">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./main.php">소마데모</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="./main.php">Home</a></li>
                <li class="active"><a href="./manage.php">manage</a></li>
                <li><a href="./manage_rule.php">rule managing</a></li>
                <li><a href="./review_semantic_result.php">semantic result</a></li>
                <li><a href="./rule_coverage.php">rule coverage</a></li>
                <li><a href="./chart.php">chart</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<div>
    <table class="table table-striped" style="width:100%;margin-top:10px;" id="info">
        <tbody>
        <tr><td><b>id</b></td><td><b>sentence</b></td><td><b>origin_attr</b></td><td><b>attr</b></td><td><b>target</b></td><td><b>sentiment_type</b></td><td><b>review_id</b></td><td><b>date</b></td><td><b>sentiment_eojeol</b></td><td><b>button</b></td></tr>
        </tbody>
    </table>
    <input type="hidden" id="updateval" value="">
    <div id="dialog" title="update"></div>
</div><!-- /.container -->
</body>
</html>


