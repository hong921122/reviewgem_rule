<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>
    <script src="./js/keyboard.js"></script>
    <script src="./js/click_event.js"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        window.onload = function() {
            refreshReviewId();
        };

        function refreshReviewId() {
            var get_rsf_review_ids = $.post('./php/get_rsf_review_ids.php');
            get_rsf_review_ids.done(function(review_ids){
                $('#review_id').html(review_ids);
            });
        }

        function getReviewIdButton() {
            return $('#btn-review-id').html();
        }

        function setReviewIdButton(review_id) {
            var result = $('#btn-review-id').html(review_id);
        }

        function getModeButton() {
            return $('#btn-mode').html();
        }

        function setModeButton(mode) {
            var result = $('#btn-mode').html(mode);
        }

        function showReviewData() {
            var review_id = event.toElement.innerHTML;
            setReviewIdButton(review_id);

            var mode = getModeButton();
            var mode_array = [];
            if (mode == '저장 안된 것만') {
                mode_array.push('no result');
            } else if (mode == '틀린 것만') {
                mode_array.push('no result');
                mode_array.push('wrong');
                mode_array.push('not a rule');
            } else {
                mode_array.push('no result');
                mode_array.push('wrong');
                mode_array.push('not a rule');
                mode_array.push('correct');
            }

            var set_table_group = $.post('./php/set_rsf_table_group.php', { review_id : review_id, mode : mode_array });
            set_table_group.done(function(table_group) {
                var result = $('#table_group').html(table_group);
            });
        }

        function updateResult() {
            var idDivElement = event.toElement;
            var newResult = event.toElement.innerHTML;
            while(idDivElement && (idDivElement.tagName != "DIV" || !idDivElement.id))
                idDivElement = idDivElement.parentNode;
            var idValue = idDivElement.getAttribute("id");
            var buttonIdValue = idValue + "button";
            var button = $('#' + buttonIdValue);
            var returnValue = button.html(newResult);
        }

        function saveRule() {
            var idDivElement = event.toElement;
            while(idDivElement && (idDivElement.tagName != "DIV" || !idDivElement.id))
                idDivElement = idDivElement.parentNode;
            var idValue = idDivElement.getAttribute("id");
            var buttonIdValue = idValue + "button";
            var selectedValue = $('#' + buttonIdValue).html();
            var reviewIdValue = getReviewIdButton();
            var idValues = idValue.split("-");
            var originAttrValue = idValues[0];
            var sentenceIdValue = idValues[1];
            var saveRsf = $.post('./php/save_rsf_result.php', { review_id : reviewIdValue, sentence_id : sentenceIdValue,
                sentiment_type : selectedValue, origin_attr : originAttrValue});
            saveRsf.done(function(result){
                alert(result);
            });
        }

        function updateEvent(){
            var update_btn = document.getElementsByClassName(event.toElement.value);
            var t = $.post('./php/update_rule_sentiment_type.php', {id : update_btn[0].value, origin_attr : update_btn[1].value,
                sentiment_type : update_btn[2].value });
            t.done(function(data){
                alert("update success");
            });
        }

    </script>
</head>
<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 40px 15px;
    }

</style>

<body id="body">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./main.php">소마데모</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="./main.php">Home</a></li>
                <li><a href="./crawl_view.php">crawl_view</a></li>
                <li><a href="./manage.php">manage</a></li>
                <li><a href="./manage_rule.php">rule managing</a></li>
                <li class="active"><a href="./review_semantic_result.php">semantic result</a></li>
                <li><a href="./rule_coverage.php">rule coverage</a></li>
                <li><a href="./chart.php">chart</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-mode" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">저장 안된 것만</button>
    <ul id="dropdown-mode" class="dropdown-menu">
        <li><a onclick="setModeButton('전부')">전부</a></li>
        <li><a onclick="setModeButton('틀린 것만')">틀린 것만</a></li>
        <li><a onclick="setModeButton('저장 안된 것만')">저장 안된 것만</a></li>
    </ul>
</div>
<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-review-id" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"">Review-ID</button>
    <ui id="review_id" class="dropdown-menu">

    </ui>
</div>
<div id="table_group">
</div>

</body>
</html>

