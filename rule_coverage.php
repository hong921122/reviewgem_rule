<!--
 * Created by IntelliJ IDEA.
 * User: Arthur
 * Date: 2014. 5. 13.
 * Time: 오전 9:52
 */
 -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>
    <script src="./js/keyboard.js"></script>
    <script src="./js/click_event.js"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        window.onload = function() {
            refreshSubmenu();
        };

        function loadApi(chart_data, chart_name) {
            var callback = drawChart.bind(null, chart_data, chart_name);
            google.load("visualization", "1", {"callback" : callback, packages:["corechart"]});
        }

        function drawChart(chart_data, chart_name) {
            if (chart_name == 'DATE별') {
                for (var i = 1; i < chart_data.length; i++) {
                    chart_data[i][0] = chart_data[i][0].replace("2014-", "");
                }
            }
            var data = setColumnsForGoogleChart(chart_data);
//            document.getElementById("chart_div").setAttribute("style", "width:" + data_array.length * 100 + "px; height:600px;");
//            var left = $(document).outerWidth() - document.getElementById("container").offsetWidth;
//            $('body').scrollLeft(left);

            var options = {
                title: chart_name,
                legend: { position: 'top', maxLines: 3 },
                bar: { groupWidth: '75%' },
                chartArea: { left:50, top:50, width:"100%", height:400 }
            };

            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }

        function getValueAt(column, dataTable, row) {
            var value = dataTable.getFormattedValue(row, column);
            if (value == "0") return "";
            else return value;
        }

        function setColumnsForGoogleChart(data_array) {
            var data = google.visualization.arrayToDataTable(data_array);
            var view = new google.visualization.DataView(data);
            view.setColumns([0,
                1, { calc: getValueAt.bind(undefined, 1),
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation" }
            ]);
            return view;
        }

        function refreshSubmenu() {
            var mode = getModeButton();
            if (mode == 'ID별') {
                var prefix = "coverage_for_ruleid_";
            } else if (mode == 'DATE별') {
                var prefix = "coverage_for_date_";
            }
            $('#review_id').html("");
            var load_file_names = $.post('./php/load_file_names.php', {dir : "/var/www/static/coverage/",
                prefix : prefix});
            load_file_names.done(function(user_names_json){
                var user_names = JSON.parse(user_names_json);
                for(var user in user_names) {
                    $('#review_id').append("<li><a onclick=\"showRuleCoverage()\">" + user_names[user] + "</li>");
                }
            });
        }

        function getSubmenuButton() {
            return $('#btn-review-id').html();
        }

        function setSubmenuButton(review_id) {
            var result = $('#btn-review-id').html(review_id);
        }

        function getModeButton() {
            return $('#btn-mode').html();
        }

        function setModeButton(mode) {
            var result = $('#btn-mode').html(mode);
            refreshSubmenu();
        }

        function showRuleCoverage() {
            var user_name = event.toElement.innerHTML;
            setSubmenuButton(user_name);

            var mode = getModeButton();
            if (mode == 'ID별') {
                var prefix = "coverage_for_ruleid_";
            } else if (mode == 'DATE별') {
                var prefix = "coverage_for_date_";
            }

            var set_table_group = $.post('./php/set_rule_coverage_table_group.php', {dir : "/var/www/static/coverage/",
                prefix : prefix, user : user_name});
            set_table_group.done(function(table_group) {
                var return_json = JSON.parse(table_group);
                var html = return_json['html'];
                loadApi(return_json['chart'], mode);
                var result = $('#table_group').html(html);
            });
        }

    </script>
</head>
<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 40px 15px;
    }

</style>

<body id="body">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./main.php">소마데모</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="./main.php">Home</a></li>
                <li><a href="./crawl_view.php">crawl_view</a></li>
                <li><a href="./manage.php">manage</a></li>
                <li><a href="./manage_rule.php">rule managing</a></li>
                <li><a href="./review_semantic_result.php">semantic result</a></li>
                <li class="active"><a href="./rule_coverage.php">rule coverage</a></li>
                <li><a href="./chart.php">chart</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-mode" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">ID별</button>
    <ul id="dropdown-mode" class="dropdown-menu">
        <li><a onclick="setModeButton('ID별')">ID별</a></li>
        <li><a onclick="setModeButton('DATE별')">DATE별</a></li>
    </ul>
</div>
<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-review-id" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"">SubMenu</button>
    <ui id="review_id" class="dropdown-menu">

    </ui>
</div>

<div id="container">
    <div id="chart_div" style="width: 100%; height: 600px;"></div>
</div>


<div id="table_group">
</div>

</body>
</html>
