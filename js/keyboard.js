var ctrl_flag = false;
$(document).keydown(function(key){
    var key_val = parseInt(key.which, 10);
    //13 enter
    if(key_val == 13){
	var current_focus = document.activeElement;
	if(current_focus.id == "sentence" || current_focus.id == "target" || current_focus.id == "attr" || current_focus.id == "sentiment")
	    $('#attr_submit').click();
	else if(current_focus.id == "dic_sentence" || current_focus.id == "dic_target" || current_focus.id == "dic_attr" || current_focus.id  =="dic_sentiment")
	    $('#dic_submit').click();
	else if(current_focus.id == "comment")
	    $('#defer').click();
	else if(current_focus.id == "review_point" || current_focus.id == "pos_neg_point")
	    $('#judge').click();
    }

    if(key_val == 82){
	var current_focus = document.activeElement;
	if(current_focus.id == "body"){
	    $('#refresh').click();
	}
    }

    if(key_val == 84){
	var current_focus = document.activeElement;
	if(current_focus.id == "body"){
	    $('#disuse').click();
	}
    }

    if(key_val == 69){
	var current_focus = document.activeElement;
	if(current_focus.id == "body"){
	    var url = $('#url').attr('href');
    	    window.open(url, '_blank');
	}
    }

    if(key_val == 78){
	var current_focus = document.activeElement;
	if(current_focus.id == "body"){
	    $('#user_name').focus();
	}
    }
    //48~59 0~9
    else if(key_val < 58 && key_val > 47 && ctrl_flag == false)
	$('#state').val(key_val - 48);	
});

$(document).keydown(function(key){
    if(key.ctrlKey)
	ctrl_flag = true;
});

$(document).keyup(function(key){
    if(key.ctrlKey)
	ctrl_flag = false;
});

