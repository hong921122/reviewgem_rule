<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>
    <script src="./js/keyboard.js"></script>
    <script src="./js/click_event.js"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        window.onload = function() {
            var load_user_name = $.post('./php/load_user_name_for_manage_rule.php');
            load_user_name.done(function(user_names){
                var user_name_array = JSON.parse(user_names);
                $('#dropdown-user-name').html("");
                for (var i in user_name_array) {
                    var user_name = user_name_array[i];
                    var li_info = "<li><a onclick=\"readData()\">" + user_name + "</a></li>";
                    $('#dropdown-user-name').append(li_info)
                }
            });
        };

        var user_data = [];

        function readData() {
            var user_name = event.toElement.innerHTML;
            setButtonUserName(user_name);
            $('#review_id').html("");
            if (getButtonMode() == "전부") {
                var only_error = false;
            } else {
                var only_error = true;
            }
            $.post('./php/read_saved_rule_error_checking.php', { user_name:user_name, only_error : only_error }, function(data) {
                window.user_data[user_name] = JSON.parse(data);

                refreshReviewID(user_name);
            });
        }

        function selectMode() {
            var mode_name = event.toElement.innerHTML;
            setButtonMode(mode_name);
            if (mode_name == "오류만") {
                $('#dropdown-user-name').html("");
                $('#review_id').html("");
                setButtonUserName("Name");
                setButtonReviewID("Review-ID");
                var load_user_name = $.post('./php/load_user_name_for_manage_rule.php', {only_error : true});
                load_user_name.done(function(user_names){
                    var user_name_array = JSON.parse(user_names);
                    for (var i in user_name_array) {
                        var user_name = user_name_array[i];
                        var li_info = "<li><a onclick=\"readData()\">" + user_name + "</a></li>";
                        $('#dropdown-user-name').append(li_info)
                    }
                });
            } else if (mode_name == "전부") {
                $('#dropdown-user-name').html("");
                $('#review_id').html("");
                setButtonUserName("Name");
                setButtonReviewID("Review-ID");
                var load_user_name = $.post('./php/load_user_name_for_manage_rule.php', {only_error : false});
                load_user_name.done(function(user_names){
                    var user_name_array = JSON.parse(user_names);
                    for (var i in user_name_array) {
                        var user_name = user_name_array[i];
                        var li_info = "<li><a onclick=\"readData()\">" + user_name + "</a></li>";
                        $('#dropdown-user-name').append(li_info)
                    }
                });
            }

        }

        function setButtonMode(mode_name) {
            $('#btn-mode').html(mode_name);
        }

        function getButtonMode() {
            return $('#btn-mode').html();
        }

        function setButtonUserName(user_name) {
            $('#btn-user-name').html(user_name);
        }

        function getButtonUserName() {
            return $('#btn-user-name').html();
        }

        function setButtonReviewID(review_id) {
            $('#btn-review-id').html(review_id);
        }

        function getButtonReviewID() {
            return $('#btn-review-id').html();
        }

        function addSentenceInfoToTableGroup(sentence_rule_result, index) {
            var sentence_info = "<table id=\"sentence_info" + index + "\" class=\"table table-striped\">";
            sentence_info += "<tbody>";
            sentence_info += "<tr>";
            sentence_info += "<td style=\"width:50px;\">id</td>";
            sentence_info += "<td style=\"width:50px;\">review_id</td>";
            sentence_info += "<td style=\"width:50px;\">sentence_id</td>";
            sentence_info += "<td>sentence</td>";
            sentence_info += "</tr>";
            sentence_info += "<tr>";
            sentence_info += "<td>" + sentence_rule_result.id + "</td>";
            sentence_info += "<td>" + sentence_rule_result.review_id + "</td>";
            sentence_info += "<td>" + sentence_rule_result.sentence_id + "</td>";
            sentence_info += "<td>" + sentence_rule_result.sentence + "</td>";
            sentence_info += "</tr>";
            sentence_info += "</tbody>";
            sentence_info += "</table>";
            $('#table_group').append(sentence_info);
        }

        function addMorphemeInfoToTableGroup(sentence_rule_result, index) {
            var morpheme_info = "<table id=\"morpheme_info" + index + "\" class=\"table table-striped\">";
            morpheme_info += "<tbody>";
            morpheme_info += "<tr>";
            morpheme_info += "<td style=\"width:50px;\">accuracy</td>";
            morpheme_info += "<td style=\"width:120px;\">origin_attr</td>";
            morpheme_info += "<td>target</td>";
            morpheme_info += "<td>attr</td>";
            morpheme_info += "<td>sentiment_eojeol</td>";
            morpheme_info += "<td style=\"width:50px;\">sentiment_type</td>";
            morpheme_info += "</tr>";
            morpheme_info += "<tr>";
            morpheme_info += "<td>" + sentence_rule_result.accuracy.toFixed(2) + "</td>";
            morpheme_info += "<td>" + sentence_rule_result.origin_attr + "</td>";
            morpheme_info += "<td>" + sentence_rule_result.target + "</td>";
            morpheme_info += "<td>" + sentence_rule_result.attr + "</td>";
            morpheme_info += "<td>" + sentence_rule_result.sentiment_eojeol + "</td>";
            morpheme_info += "<td>" + sentence_rule_result.sentiment_type + "</td>";
            morpheme_info += "</tr>";
            morpheme_info += "</tbody>";
            morpheme_info += "</table>";
            $('#table_group').append(morpheme_info);
        }

        function addReviewMorphemeInfoToTableGroup(sentence_rule_result, index) {
            var morpheme_info = "<table id=\"review_morpheme_info" + index + "\" class=\"table table-striped\">";
            morpheme_info += "<tbody>";
            morpheme_info += "<tr>";
            morpheme_info += "<td>review_morpheme</td>";
            morpheme_info += "<td>review_eojeol</td>";
            morpheme_info += "</tr>";
            morpheme_info += "<tr>";
            morpheme_info += "<td>";
            morpheme_info += "[";
            for (var i in sentence_rule_result.review_morpheme) {
                if (i != 0)
                    morpheme_info += ", ";
                morpheme_info += "[";
                for (var j in sentence_rule_result.review_morpheme[i]) {
                    if (j != 0)
                        morpheme_info += ", ";
                    morpheme_info += sentence_rule_result.review_morpheme[i][j];
                }
                morpheme_info += "]";
            }
            morpheme_info += "]";
            morpheme_info += "</td>";
            morpheme_info += "<td>" + sentence_rule_result.review_eojeol + "</td>";
            morpheme_info += "</tr>";
            morpheme_info += "</tbody>";
            morpheme_info += "</table>";
            $('#table_group').append(morpheme_info);
        }

        function addNotFoundListInfoToTableGroup(not_found_result) {
            var not_found_list_info = "<table class=\"table table-striped\">";
            not_found_list_info += "<tbody>";
            not_found_list_info += "<tr>";
            not_found_list_info += "<td style=\"width:50px;\">accuracy</td>";
            not_found_list_info += "<td>not_found_list</td>";
            not_found_list_info += "</tr>";
            not_found_list_info += "<tr>";
            not_found_list_info += "<td>" + (not_found_result.count_of_contains / not_found_result.length).toFixed(2) + "</td>";
            not_found_list_info += "<td>";
            not_found_list_info += "[";
            for (var i in not_found_result.not_found_list) {
                if (i != 0)
                    not_found_list_info += ", ";
                not_found_list_info += not_found_result.not_found_list[i];
            }
            not_found_list_info += "]";
            not_found_list_info += "</td>";
            not_found_list_info += "</tr>";
            not_found_list_info += "</tbody>";
            not_found_list_info += "</table>";
            $('#table_group').append(not_found_list_info);
        }

        function addRuleErrorCheckingInfoToTableGroup(sentence_rule_result, index) {
            if (sentence_rule_result.target_result.length > 0 &&
                sentence_rule_result.target_result.count_of_contains != sentence_rule_result.target_result.length) {
                $('#table_group').append("<label>target_result</label><br>");
                addNotFoundListInfoToTableGroup(sentence_rule_result.target_result);
            }
            if (sentence_rule_result.attr_result.length > 0 &&
                sentence_rule_result.attr_result.count_of_contains != sentence_rule_result.attr_result.length) {
                $('#table_group').append("<label>attr_result</label><br>");
                addNotFoundListInfoToTableGroup(sentence_rule_result.attr_result);
            }
            if (sentence_rule_result.sentiment_eojeol_result.length > 0 &&
                sentence_rule_result.sentiment_eojeol_result.count_of_contains != sentence_rule_result.sentiment_eojeol_result.length) {
                $('#table_group').append("<label>sentiment_eojeol_result</label><br>");
                addNotFoundListInfoToTableGroup(sentence_rule_result.sentiment_eojeol_result);
            }
        }

        function addRecommendedResultBaseInfoToTableGroup(recommended_result) {
            var recommended_result_base_info = "<table class=\"table table-striped\">";
            recommended_result_base_info += "<tbody>";
            recommended_result_base_info += "<tr>";
            recommended_result_base_info += "<td style=\"width:50px;\">accuracy</td>";
            recommended_result_base_info += "<td style=\"width:50px;\">sentence_id</td>";
            recommended_result_base_info += "<td>sentence</td>";
            recommended_result_base_info += "<td>review_morpheme</td>";
            recommended_result_base_info += "<td>review_eojeol</td>";
            recommended_result_base_info += "</tr>";
            recommended_result_base_info += "<tr>";
            recommended_result_base_info += "<td>" + recommended_result.accuracy.toFixed(2) + "</td>";
            recommended_result_base_info += "<td>" + recommended_result.sentence_id + "</td>";
            recommended_result_base_info += "<td>" + recommended_result.sentence + "</td>";
            recommended_result_base_info += "<td>";
            recommended_result_base_info += "[";
            for (var i in recommended_result.review_morpheme) {
                if (i != 0)
                    recommended_result_base_info += ", ";
                recommended_result_base_info += "[";
                for (var j in recommended_result.review_morpheme[i]) {
                    if (j != 0)
                        recommended_result_base_info += ", ";
                    recommended_result_base_info += recommended_result.review_morpheme[i][j];
                }
                recommended_result_base_info += "]";
            }
            recommended_result_base_info += "]";
            recommended_result_base_info += "</td>";
            recommended_result_base_info += "<td>" + recommended_result.review_eojeol + "</td>";
            recommended_result_base_info += "</tr>";
            recommended_result_base_info += "</tbody>";
            recommended_result_base_info += "</table>";
            $('#table_group').append(recommended_result_base_info);
        }

        function addRecommendedResultInfoToTableGroup(sentence_rule_result) {
            if (sentence_rule_result.other_results && sentence_rule_result.other_results.length > 0) {
                $('#table_group').append("<label>추천 문장 리스트</label><br>");
                for (var i in sentence_rule_result.other_results) {
                    var other_result = sentence_rule_result.other_results[i];
                    addRecommendedResultBaseInfoToTableGroup(other_result);
                }
            }
        }

        function addUpdateFieldToTableGroup(field_data) {
            var update_field_info = "<table class=\"table table-striped\">";
            update_field_info += "<tbody>";
            update_field_info += "<tr>";
            update_field_info += "<td style=width:120px>update</td>";
            update_field_info += "<td style=width:50px>insert</td>";
            update_field_info += "<td style=width:50px>id</td>";
            update_field_info += "<td style=width:50px>review_id</td>";
            update_field_info += "<td style=width:50px>sentence_id</td>";
            update_field_info += "<td>sentence</td>";
            update_field_info += "</tr>";
            update_field_info += "<tr>";
            update_field_info += "<td><button type=\"submit\" onclick=updateEvent(event) value=\"" + field_data.id + "\">update</button></td>";
            update_field_info += "<td><button type=\"submit\" onclick=insertEvent(event) value=\"" + field_data.id + "\">insert</button></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.id + "\" readonly=\"readonly\" style=width:50px></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.review_id + "\" readonly=\"readonly\" style=width:50px></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.sentence_id +"\" style=width:50px></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.sentence + "\" style=width:600px></td>";
            update_field_info += "</tr>";
            update_field_info += "<td>origin_attr</td>";
            update_field_info += "<td>target</td>";
            update_field_info += "<td>attr</td>";
            update_field_info += "<td>sentiment_eojeol</td>";
            update_field_info += "<td>sentiment_type</td>";
            update_field_info += "<td>date</td>";
            update_field_info += "</tr>";
            update_field_info += "<tr>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.origin_attr + "\" style=width:120px></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.target + "\"></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.attr + "\"></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.sentiment_eojeol + "\"></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.sentiment_type + "\" style=width:50px></td>";
            update_field_info += "<td><input type=\"text\" class=\"" + field_data.id + "\" value=\"" + field_data.date + "\" readonly=\"readonly\"></td>";
            update_field_info += "</tr>";
            update_field_info += "</tbody>";
            update_field_info += "</table>";

            $('#table_group').append(update_field_info);
        }

        function insertEvent(event) {
            var insert_btn = document.getElementsByClassName(event.toElement.value);
            if (confirm("really insert?")) {
                var t = $.post('./php/attr_submit.php', {review_id : insert_btn[1].value, sentence_id : insert_btn[2].value,
                    sentence : insert_btn[3].value, origin_attr : insert_btn[4].value, target : insert_btn[5].value, attr : insert_btn[6].value,
                    sentiment : insert_btn[7].value, sentiment_type : insert_btn[8].value, add_parenthesis : 'false' } );
                t.done(function(data){
                    alert("insert sucess");
                });
            }
        }

        function updateEvent(event){
            var update_btn = document.getElementsByClassName(event.toElement.value);
            var t = $.post('./php/req.php', {id : update_btn[0].value, review_id : update_btn[1].value, sentence_id : update_btn[2].value,
                sentence : update_btn[3].value, origin_attr : update_btn[4].value, target : update_btn[5].value, attr : update_btn[6].value,
                sentiment_eojeol : update_btn[7].value, sentiment_type : update_btn[8].value });
            t.done(function(data){
                alert("update success");
            });
        }

        function showReviewData(event_id) {
            if (event_id == "dropdown-mode") return;
            var review_id = event.toElement.innerHTML;
            var user_name = getButtonUserName();
            setButtonReviewID(review_id);
            var rule_data = window.user_data[user_name][review_id];
            var review_content = rule_data['content'];
            var rule_result = rule_data['result'];
            var id_list = [];
            for (var i in rule_result) {
                id_list.push(rule_result[i].id);
            }
            $.post('./php/manage_rule_update_field.php', { id_list:id_list.toString() }, function(field_info) {
                var field_info_json = JSON.parse(field_info);
                $('#table_group').html("");
                for (var i in rule_result) {
                    $('#table_group').append("<label>저장된 룰 정보</label><br>");
                    addSentenceInfoToTableGroup(rule_result[i], i);
                    addMorphemeInfoToTableGroup(rule_result[i], i);
                    $('#table_group').append("<label>원본 리뷰 문장 정보</label><br>");
                    addReviewMorphemeInfoToTableGroup(rule_result[i], i);
                    $('#table_group').append("<label>룰 에러 채킹 결과</label><br>");
                    addRuleErrorCheckingInfoToTableGroup(rule_result[i], i);
                    addRecommendedResultInfoToTableGroup(rule_result[i]);
                    var rule_id = rule_result[i].id.toString();
                    addUpdateFieldToTableGroup(field_info_json[rule_id]);
                    $('#table_group').append("<HR style=\"width:100%;height:10px;background-color:red;\"/>");
                }
            });
        }

        function refreshReviewID(user_name) {
            var user_name = getButtonUserName();
            $('#review_id').html("");
            for (var i in window.user_data[user_name]) {
                $('#review_id').append("<li><a onclick=\"showReviewData()\">" + i + "</li>");
            }
        }

    </script>
</head>
<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 40px 15px;
    }

</style>

<body id="body">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./main.php">소마데모</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="./main.php">Home</a></li>
                <li><a href="./crawl_view.php">crawl_view</a></li>
                <li><a href="./manage.php">manage</a></li>
                <li class="active"><a href="./manage_rule.php">rule managing</a></li>
                <li><a href="./review_semantic_result.php">semantic result</a></li>
                <li><a href="./rule_coverage.php">rule coverage</a></li>
                <li><a href="./chart.php">chart</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-mode" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">오류만</button>
    <ul id="dropdown-mode" class="dropdown-menu">
        <li><a onclick="selectMode()">오류만</a></li>
        <li><a onclick="selectMode()">전부</a></li>
    </ul>
</div>
<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-user-name" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">Name</button>
    <ul id="dropdown-user-name" class="dropdown-menu">
        <li><a onclick="readData()">유은상</a></li>
        <li><a onclick="readData()">박진철</a></li>
        <li><a onclick="readData()">홍석준</a></li>
    </ul>
</div>
<div class="btn-group" style="margin-top:10px;margin-left:5px;">
    <button id="btn-review-id" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"">Review-ID</button>
    <ul id="review_id" class="dropdown-menu">

    </ul>
</div>
<div id="table_group">
</div>

</body>
</html>

