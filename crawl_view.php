<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>뿌잉뿌잉</title>
    <link href="./css/bootstrap-3.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="./css/bootstrap-3.1.0/dist/js/bootstrap.min.js"></script>
    <script src="http://58.229.6.137:8080/js/keyboard.js"></script>


</head>
<script type="text/javascript">
/*
 window.onload = function(){
 var posting = $.post('./php/getpost.php', function(data){
 console.log(data);
 var t = data.split("\n");
 var str = t[t.length-1];
 var rand_num = str.substring(str.lastIndexOf("value") + 7, str.length-2);
 $('#body').append(str);

 data = data.substring(0, data.lastIndexOf("\n"));
 var res = JSON.parse(data);
 $('#info1').append(res.content);
 $('#title').append(res.title);
 $('#review_id').append(rand_num);
 $('#url').append(res.url);
 $('#url').attr('href', res.url);
 var val = $('#rand').val();
 var defer_chk = $.post('./php/defer_chk.php', {value : val});
 defer_chk.done(function(data){
 $('#defer_comm').append(data);
 });
 $('#morpheme_label').append("morpheme (" + val + ")");
 var mor = $.post('./php/morpheme.php', {value : val});
 mor.done(function(data){
 $('#morpheme').append(data);
 });
 });
 };
 */
$(document).ready(function(){
    $('#judge').click(function(event){
        console.log("judge");
        event.preventDefault();
        var val = $('#rand').val();
        var user_name = $('#user_name').val();
        var r_review_point = document.getElementById('review_point').value;
        var r_pos_neg_point = document.getElementById('pos_neg_point').value;

        var posting = $.post('./php/submit.php', {review_point : r_review_point, pos_neg_point : r_pos_neg_point, user : user_name, value : val});
        posting.done(function(data){
            setContents(0);
        });

        $('#review_point').val("");$('#pos_neg_point').val("");
        $('html, body').animate({scrollTop: $('#body').offset().top});
    });

    $('#dic_submit').click(function(event){
        var dic_sentence_id = $('#sentence_id').attr('value');
        var dic_review_id = $('#rand').val();
        var dic_attr = $('#dic_attr').val();
        var dic_target = $('#dic_target').val();
        var dic_sentiment = $('#dic_sentiment').val();
        var dic_submit = $.post('./php/dic_submit.php', {sentence_id:dic_sentence_id, review_id:dic_review_id, attr:dic_attr, target:dic_target, sentiment:dic_sentiment});
        $('#dic_sentence').val("");$('#dic_attr').val("");$('#dic_target').val("");$('#dic_sentiment').val("");
        dic_submit.done(function(data){
            alert("dic submit success");
        });
    });

    $('#attr_submit').click(function(event){
        var r_sentiment_type = $('#btn-sentiment').html();
        var r_review_id = $('#rand').val();
        var r_origin_attr = $('#origin_attr').html();
        var r_sentence = $('#sentence').val();
        var r_attr = $('#attr').val();
        var r_sentiment = $('#sentiment').val();
        var r_target = $('#target').val();
        var r_sentence_id = $('#sentence_id').attr('value');

        var attr_submit = $.post('./php/attr_submit.php', {sentence:r_sentence, attr:r_attr, target:r_target, sentiment:r_sentiment, origin_attr:r_origin_attr, sentiment_type:r_sentiment_type, review_id:r_review_id, sentence_id:r_sentence_id});
        $('#sentence').val("");$('#attr').val("");$('#target').val("");$('#sentiment').val("");
        attr_submit.done(function(data){
            alert("rule submit success");
        });
    });

    $('#defer').click(function(event){
        var val = $('#rand').val();
        var comm = $('#comment').val();
        var debug = $.post('./php/defer.php', {value:val, comment: comm});
        debug.done(function(data){
            alert("done");
        });
        debug.fail(function(data){
            alert(data.responseText);
        });
    });

    $('#disuse').click(function(event){
        var d = $('#rand').val();
        $.post('./php/disuse.php', {disuse: d});
        $('#refresh').click();
    });

    $('#refresh').click(function(event){
        setContents(0);
        $('html, body').animate({scrollTop: $('#body').offset().top});
    });

    $('#select_review').click(function(event){
        var review_id = $('#input_review_id').val();
        setContents(review_id);
    });

    $('#select_complete').click(function(event){
        $('#refresh').click();
    });

    $('#category').change(function(){
        var v = "test";
        var pro = $.post('./php/getproduct.php', {value : v}, function(data){
            $('#product').html("");
            $('#product').append(data);
        });
    });

});

function toArray(obj) {
    x = [];
    $.each(obj, function(i, n){
        x.push(n);
    });
    return x;
}

function getEojeol(elements) {
    var eojeol_arr = toArray(elements);
    $('#morpheme').append("<br>");
    $('#morpheme').append("[어절] : [");
    for(var eojeol in eojeol_arr) {
        var eojeol_el = $("<a />", {
            "name" : "wordlist",
            "draggable" : true,
            "ondragstart" : "drag(event)",
            text : eojeol_arr[eojeol]
        });
        $('#morpheme').append(eojeol_el);
        $('#morpheme').append(", ");
    }
    $('#morpheme').append("]");
}

function getMorpheme(morpheme, tag) {
    $('#morpheme').append("<br>[형태소/한글태그명] : ");
    var morphemes_arr = toArray(morpheme);
    var tags_arr = toArray(tag);

    for(var idx in morphemes_arr) {
        var morpheme_arr = toArray(morphemes_arr[idx]);
        var tag_arr = toArray(tags_arr[idx]);
        $('#morpheme').append(" [");
        for(var _idx in morpheme_arr) {
            $('#morpheme').append(morpheme_arr[_idx] + "/" + tag_arr[_idx] + ", ");
        }
        $('#morpheme').append("] ");
    }
}

function replaceIdx(sentence, start, end, src) {
    var start_str = sentence.substring(0, start);
    var end_str = sentence.substring(end, sentence.length);

    return start_str + src + end_str;
}

function setHighlight(sentence, highlight) {
    var highlight_arr = JSON.parse(highlight);
    highlight_arr.sort(function(a, b){
        return a[0] > b[0] ? -1 : 1;
    });
    highlight_arr.forEach(function(elem){
        var element = $("<font />", {
            "style" : "background-color:#00FF00",
            text : sentence.substring(elem[0], Number(elem[1])+1)
        });
        sentence = replaceIdx(sentence, Number(elem[0]), Number(elem[1])+1, element[0].outerHTML);
        //sentence = sentence.replace(new RegExp(sentence.substring(elem[0], elem[1]) + '$'), element[0].outerHTML);
    });

    return sentence;
}

function setContents(select_num)
{
    var crawl_source = $('#crawl_source').val();
    var product = $('#product').val();
    var category = $('#category').val();

    var getdoc = $.post('./php/get_morpheme_post.php', {select_number : select_num, input_source : crawl_source, input_product : product, input_category : category});
    getdoc.done(function(data){
        console.log(data);
        document.getElementById('info1').innerHTML = "";
        document.getElementById('title').innerHTML = "";
        document.getElementById('url').innerHTML = "";
        document.getElementById('review_id').innerHTML = "";
        $('#morpheme').html("");

        $('#info1').append(data.info.content);
        $('#review_id').append(data.value);
        $('#title').append(data.info.title);
        $('#url').append(data.info.url);
        $('#url').attr('href', data.info.url);
        $('#rand').val(data.value);

        $('#morpheme').append("<br><br>");
        for(var e in data.info.morphemeAnalyze) {
            $('#morpheme').append(Number(e)+1 + ": ");
            if(data.info.morphemeAnalyze[e].highlighting){
                $('#morpheme').append(setHighlight(data.info.morphemeAnalyze[e].sentence, data.info.morphemeAnalyze[e].highlighting));
            }
            else
                $('#morpheme').append(data.info.morphemeAnalyze[e].sentence);
            /*
             var el = $("<pre />", {
             text : (data.info.morphemeAnalyze[e].highlighting) ? setHighlight(data.info.morphemeAnalyze[e].sentence, data.info.morphemeAnalyze[e].highlighting) : data.info.morphemeAnalyze[e].sentence
             });

             var el = $("<a />", {
             "onclick" : "setSentence(event)",
             "value" : Number(e) + 1,
             "style" : (data.info.morphemeAnalyze[e].highlighting) ? "background-color:#00FF00" : "" ,
             text : data.info.morphemeAnalyze[e].sentence
             });
             */
            var btn = $("<button />", {
                "onclick" : "setSentence(event)",
                "value" : data.info.morphemeAnalyze[e].sentence,
                "sentence_number" : Number(e) + 1,
                text : "sentence"
            });
//            $('#morpheme').append(el);
            $('#morpheme').append(btn);
            getMorpheme(data.info.morphemeAnalyze[e].morphemes, data.info.morphemeAnalyze[e].koreantags);
            getEojeol(data.info.morphemeAnalyze[e].eojeol);
            $('#morpheme').append("<br><br><br>");
        }
//            var val = $('#rand').val();
        var rand_num = $('#rand').val();
        var defer_chk = $.post('./php/defer_chk.php', {value:rand_num});
        defer_chk.done(function(data){
            $('#defer_comm').append(data);
        });
        /*
         $('#morpheme_label').html("");
         $('#morpheme_label').append("morpheme (" + rand_num + ")");
         var mor = $.post('./php/morpheme.php', {value : rand_num});
         mor.done(function(data){
         document.getElementById('morpheme').innerHTML= "";
         $('#morpheme').append(data);
         });
         */
    });
    getdoc.fail(function(data, textStatus, errorThrown){
        console.log(errorThrown);
        //console.log(data);
    });
}

function setSentence(event)
{
    var selector = "a[value='" + $('#sentence_id').val() + "']";
    $(selector).css("color", "#2a6496");
    $('a').css("backgroundColor", "transparent");
    $('#sentence').val(event.toElement.value);
    $('#dic_sentence').val(event.toElement.value);
    event.toElement.style.backgroundColor = "#FFFFFF";
    event.toElement.style.color = "#FF0000";
    document.getElementById('sentence_id').value = event.toElement.attributes.sentence_number.value;
}

function allowDrop(event)
{
    event.preventDefault();
}

function drag(event)
{
    event.dataTransfer.setData("Text", event.target.text);
}

function drop(event)
{
    var drop_text = event.dataTransfer.getData("Text");
    event.target.text = drop_text;
}

function getSentiment(event)
{
    $('#btn-sentiment').html("");
    $('#btn-sentiment').html(event.toElement.innerHTML);
}

function getAttr(event)
{
    $('#origin_attr').html("");
    $('#origin_attr').html(event.toElement.innerHTML);
}
</script>

<style>
    body {
        padding-top: 50px;
    }
    .template {
        padding: 10px 15px;
    }
    .content {
        width: 500px;
        height: 500px;
    }
    .info {
        margin-top:10px;
        width: 50%;
        float: left;
    }
    .custom-container{
        width: 100%;
        margin-left: 100px;
    }
</style>

<body id="body">

<div class="navbar navbar-inverse navbar-fixed-top" style="height:100px;" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="btn-group" style="margin-top:10px;margin-right:5px;">
                <button id="origin_attr" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                    ATTR <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a onclick="getAttr(event)">기타</a></li>
                    <li><a onclick="getAttr(event)">배터리</a></li>
                    <li><a onclick="getAttr(event)">디스플레이</a></li>
                    <li><a onclick="getAttr(event)">해상도</a></li>
                    <li><a onclick="getAttr(event)">카메라</a></li>
                    <li><a onclick="getAttr(event)">휴대폰 자체 성능</a></li>
                    <li><a onclick="getAttr(event)">휴대성</a></li>
                    <li><a onclick="getAttr(event)">가격</a></li>
                    <li><a onclick="getAttr(event)">디자인</a></li>
                    <li><a onclick="getAttr(event)">그립감</a></li>
                    <li><a onclick="getAttr(event)">내구성</a></li>
                </ul>
            </div>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav" style="">
                <li><input type="text" class="form-control" placeholder="문장" id="sentence" style="width:500px;margin-top:10px;margin-right:5px;float:left;"/></li>
                <li><input type="text" class="form-control" placeholder="타겟" id="target" style="width:100px;margin-top:10px;margin-right:5px;float:left;"/></li>
                <li><input type="text" class="form-control" placeholder="속성" id="attr" style="width:100px;margin-top:10px;margin-right:5px;margin-left:5px;float:left;"/></li>
                <li><input type="text" class="form-control" placeholder="감성" id="sentiment" style="width:100px;margin-top:10px;margin-left:5px;float:left;"/></li>
                <li><button id="attr_submit" class="btn btn-default" style="margin-top:10px;margin-left:5px;float:left;">submit</button></li>
            </ul>
            <div class="btn-group" style="margin-top:10px;margin-left:5px;">
                <button id="btn-sentiment" class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                    ATTR<span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a onclick="getSentiment(event)">positive</a></li>
                    <li><a onclick="getSentiment(event)">neutral</a></li>
                    <li><a onclick="getSentiment(event)">negative</a></li>
                </ul>
            </div>

        </div><!--/.nav-collapse -->
        <div>
            <ul class="nav navbar-nav">
                <li><input type="text" class="form-control" placeholder="이름" id="user_name" style="margin-top:10px;width:70px;float:left;"></li>
                <li><a>사전등록</a></li>
                <li><input type="text" class="form-control" placeholder="문장" id="dic_sentence" style="width:500px;margin-top:10px;margin-right:5px;float:left;"/></li>
                <li><input type="text" class="form-control" placeholder="타겟" id="dic_target" style="margin-top:10px;margin-right:5px;width:100px;float:left;"/></li>
                <li><input type="text" class="form-control" placeholder="속성" id="dic_attr" style="margin-top:10px;margin-right:5px;width:100px;float:left;"/></li>
                <li><input type="text" class="form-control" placeholder="감성" id="dic_sentiment" style="width:100px;margin-top:10px;margin-left:5px;margin-right:5px;float:left;"/></li>
                <li><button id="dic_submit" class="btn btn-default" style="margin-top:10px;margin-left:5px;float:left;">submit</button></li>
            </ul>
        </div>
    </div>
    <ul class="nav navbar-nav" style="background-color:#222;">
        <li><a href="./main.php">Back to Home</a></li>
    </ul>
</div>

<!--<div style="float:left;width:30%;height:100%;margin-top:20px;margin-left:10px;margin-right:10px;">
<label id="state" for="state" style="margin-top:100px;"></label>
<pre id="morpheme" style="height:100%;margin-top:10px;">
</pre>
</div>-->

<div class="container" style="margin-top:50px">
    <!--<div class="iframe">
   <iframe name="content" style="width:100%;height:600px;margin-top:20px" id="content">
   </iframe>
</div>-->

    <div id="defer_comm" style="margin-top:5px;">
    </div>
    <div class="info" style="width:100%;">
        <div style="margin-bottom:10px;">
            <input type="text" placeholder="리뷰 아이디" id="input_review_id">
            <button type="submit" id="select_review">리뷰 가져오기</button>
            <label>카테고리</label>
            <select id="category">
                <option value="phone">핸드폰</option>
                <option value="notebook">노트북</option>
                <option value="camera">카메라</option>
            </select>
            <label>리뷰 소스</label>
            <select id="crawl_source">
                <option value="naver">네이버</option>
                <option value="ppomppu">뽐뿌</option>
            </select>
            <label>제품 종류</label>
            <select id="product">
            </select>
            <button type="submit" id="select_complete">complete</button>
        </div>
        <label>review id</label>
        <pre id="review_id" style="margin-top:5px;"></pre>
        <label>title</label>
        <button id="disuse" style="float:right;margin-left:10px;">폐기</button>
        <button id="refresh" style="float:right;">refresh</button>
        <pre id="title" style="margin-top:5px;"></pre>
        <title>URL</title>
        <pre style="mrigin-top:5px;width:100%;"><a id="url" href="" target="_blank"></a></pre>
    </div>
    <div style="margin:0 auto;">
        <div style="width:50%;float:left;">
            <label>content</label>
		<pre id="info1">
		</pre>
        </div>
        <div style="width:50%;float:left;">
            <label id="morpheme_label"></label>
	        <pre id="morpheme">
		</pre>
        </div>
    </div>

    <div>
        <form role="form">
            <div class="form-group">
                <label for="inputPositive" style="width:100%;">리뷰도</label>
                <input type="text" class="form-control" placeholder="리뷰 점수" id="review_point">
            </div>
            <div class="form-group">
                <label for="inputNegative">장/단점도</label>
                <input type="text" class="form-control" placeholder="장/단점 점수" id="pos_neg_point">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default" style="float:right;margin-bottom:10px;" id="judge">Submit</button>
            </div>
        </form>
        <br><br>
        <form>
            <div class="form-group">
                <label for="comment">보류이유</label>
                <input type="text" class="form-control" placeholder="보류" id="comment">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default" style="float:right;margin-bottom:10px;" id="defer">submit</button>
            </div>
        </form>
    </div>
    <input type="hidden" id="sentence_id" name="sentence_id" value=""/>
    <input type="hidden" id="source" name="source" value=""/>
    <input type="hidden" id="rand" value=""/>
</div><!-- /.container -->

</body>
</html>

